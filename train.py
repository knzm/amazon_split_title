# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
import os
import random
import json

try:
    import svm
except ImportError:
    sys.path.insert(0, 'libsvm-python')

from svm import svm_problem, svm_parameter
from svmutil import svm_train, svm_predict, svm_save_model

from common import make_vector, save_words


def make_svm_data(dataset, word_ids, categories):
    xi = []
    for data in dataset:
        for d in data['sequence']:
            feature = {
                'forward': d['forward_features'],
                'current': d['current_features'],
                'backward': d['backward_features'],
                }
            vec = make_vector(feature, word_ids)
            xi.append(vec)

    yi = []
    for data in dataset:
        for d in data['sequence']:
            yi.append(d['category'] in categories)

    return yi, xi


def partition_dataset(dataset):
    eval_indices = random.sample(range(len(dataset)), len(dataset) / 5)
    eval_dataset = [dataset[i] for i in eval_indices]

    train_dataset = [dataset[i] for i in xrange(len(dataset))
                     if i not in eval_indices]

    return train_dataset, eval_dataset


def build_word_ids(dataset):
    words = set()
    for data in dataset:
        for d in data['sequence']:
            words.update(d['forward_features'])
            words.update(d['current_features'])
            words.update(d['backward_features'])

    word_ids = {}
    for wid, w in enumerate(sorted(words), 1):
        word_ids[w] = wid
    return word_ids


def main(dataset, skip_test=True):
    word_ids = build_word_ids(dataset)
    save_words(os.path.join('data/words.txt'), word_ids)

    # -t kernel_type (0 = linear)
    # -c cost
    # -h shrinking
    # -b probability_estimates
    # -m cache size (1GB)
    param = svm_parameter('-t 0 -c 3 -h 1 -b 1 -m 1024')

    if skip_test:
        train_dataset, eval_dataset = dataset, None

    else:
        print("=== Start sampling data ===")
        train_dataset, eval_dataset = partition_dataset(dataset)
        print("=== Finish sampling data ===")

    for category in xrange(4):
        yi, xi = make_svm_data(train_dataset, word_ids, [category])
        prob = svm_problem(yi, xi)

        print("=== Start trainging ({0}) ===".format(category))
        m = svm_train(prob, param)
        print("=== Finish trainging ({0}) ===".format(category))

        svm_save_model('data/category-{0}.model'.format(category), m)

        if skip_test:
            continue

        print("=== Start self check ({0}) ===".format(category))
        p_label, p_acc, p_vals = svm_predict(yi, xi, m)
        print("=== Finish self check ({0}) ===".format(category))

        yt, xt = make_svm_data(eval_dataset, word_ids, [category])

        print("=== Start prediction ({0}) ===".format(category))
        p_label, p_acc, p_vals = svm_predict(yt, xt, m)
        print("=== Finish prediction ({0}) ===".format(category))


if __name__ == '__main__':
    import sys
    with open(sys.argv[1]) as f:
        main(json.load(f))
