# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
from collections import OrderedDict
import csv
import json

import zenhan

from dpmatch import DPMatcher, Match
from common import extract_features


def is_whitespace(s):
    return s.strip() == ""

def is_symbol(s):
    s = normalize(s)
    if s in r"-\"'":
        return True
    elif s in u"。、・":
        return True
    else:
        return False

def normalize(s):
    s = zenhan.z2h(s).strip()
    if s in (u'\u2014', u'\u2015'):
        # hyphen / dash
        s = '-'
    if s == u'\u203e':
        # tilde
        s = '~'
    if s == u"\u25cb":
        # circle
        s = u"\u3007"
    return s


class TitleMatcher(DPMatcher):
    def cost(self, c_key, c_text, i, j):
        if is_whitespace(c_key) and is_whitespace(c_text):
            return 0
        elif normalize(c_key) == normalize(c_text):
            return 0
        elif is_whitespace(c_key) and is_symbol(c_text):
            return 0.5
        elif is_whitespace(c_text) and is_symbol(c_key):
            return 0.5
        elif c_key and c_text:
            return 2
        else:
            return 1


def match_title(title, series):
    matcher = TitleMatcher(series, title)
    result = matcher.match()

    if result.match_count == 0:
        return None, None

    max_cost = max(0.2 * len(result.matched), 2.5)
    if result.cost < max_cost:
        if isinstance(result.tokens[0], Match):
            return result.start, result.end
        else:
            return 0, result.end

    return None, None


def get_category(pos, start, end):
    if pos == start:
        # entering region
        return 0
    elif pos == end:
        # leaveing region
        return 1
    elif start <= pos <= end:
        # in the region
        return 2
    else:
        # out of region
        return 3


def main(f, wf):
    reader = csv.reader(f)

    dataset = []
    for data_id, row in enumerate(reader):
        isbn, title, series = map(lambda s: s.decode('utf-8'), row)

        features = extract_features(title)

        if series in title:
            start = title.index(series)
            end = start + len(series)
        else:
            start, end = match_title(title, series)
            if start is None or end is None:
                print(u"not matched: {0} for {1}".format(series, title),
                      file=sys.stderr)
                continue
            print(u"dp match: {0}".format(title[start:end]), file=sys.stderr)

        sequence = []
        for i in xrange(len(title)+1):
            d = OrderedDict([
                ("position", i),
                ("category", get_category(i, start, end)),
                ("forward_features", features[i]['forward']),
                ("current_features", features[i]['current']),
                ("backward_features", features[i]['backward']),
            ])
            sequence.append(d)

        d = OrderedDict([
            ("data_id", data_id),
            ("text", title),
            ("start", start),
            ("end", end),
            ("sequence", sequence),
        ])
        dataset.append(d)

    s = json.dumps(dataset, indent=2, ensure_ascii=False)
    print(s.encode('utf-8'), file=wf)


if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        with open(sys.argv[2], 'w') as wf:
            main(f, wf)
