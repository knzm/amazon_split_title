# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
import re
import csv

import zenhan

try:
    import svm
except ImportError:
    sys.path.insert(0, 'libsvm-python')

from svmutil import svm_load_model

from common import make_vector, read_words, extract_features, svm_predict_values


def normalize(text):
    text = zenhan.z2h(text, mode=zenhan.ASCII|zenhan.DIGIT)
    text = re.sub(ur'\s+', " ", text)
    text = re.sub(ur'([^a-zA-Z0-9])!', lambda m: m.group(1)+u"！", text)
    return text


def make_vectors(text, word_ids):
    vectors = []

    for feature in extract_features(text):
        vec = make_vector(feature, word_ids)
        vectors.append(vec)

    return vectors


def predict(x, m):
    pos = {}
    for i, xi in enumerate(x):
        y, value = svm_predict_values(m, xi)
        if y > 0:
            pos[i] = value

    return pos


def main(f):
    words = read_words("data/words.txt")

    word_ids = {}
    for wid, w in words.items():
        word_ids[w] = wid

    end_model = svm_load_model("data/category-1.model")
    start_model = svm_load_model("data/category-0.model")

    reader = csv.reader(f)

    for i, row in enumerate(reader):
        isbn, title, series = map(lambda s: s.decode('utf-8'), row)

        vectors = make_vectors(title, word_ids)

        start = 0
        end = len(title) + 1

        result = predict(vectors, end_model)
        end_candidates = [pos for pos in sorted(result.keys()) if pos > 0]
        if len(end_candidates) > 0:
            end = end_candidates[0]

        result = predict(vectors, start_model)
        start_candidates = [pos for pos in sorted(result.keys()) if pos > 0]
        if len(start_candidates) > 0:
            pos = start_candidates[0]
            if pos < end and result[pos] > 0.9:
                start = pos

        normalized_title = normalize(title[start:end])
        if not normalized_title:
            normalized_title = normalize(title[:end])

        print(start, end, normalized_title.encode('utf-8'))


if __name__ == '__main__':
    import sys
    if len(sys.argv) == 1:
        main(sys.stdin)
    else:
        for path in sys.argv[1:]:
            with open(path) as f:
                main(f)
