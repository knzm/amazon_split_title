# -*- coding: utf-8 -*-

"""
Matching Algorithm using Dynamic Programing

ref. http://chalow.net/2007-01-22-4.html
"""

from __future__ import print_function


class DPMatchToken(object):
    def __init__(self, cost, key, text):
        self.cost = cost
        self.key = key
        self.text = text

    def __unicode__(self):
        return str(self).decode('unicode-escape')


class Match(DPMatchToken):
    def __init__(self, cost, text):
        super(Match, self).__init__(cost, text, text)

    def __str__(self):
        return "match({0!r}, {1})".format(self.key, self.cost)


class Replace(DPMatchToken):
    def __init__(self, cost, text_old, text_new):
        super(Replace, self).__init__(cost, text_old, text_new)

    def __str__(self):
        return "replace({0!r}, {1!r}, {2})".format(self.key, self.text, self.cost)

class Insert(DPMatchToken):
    def __init__(self, cost, text_new):
        super(Insert, self).__init__(cost, "", text_new)

    def __str__(self):
        return "insert({0!r}, {1})".format(self.text, self.cost)

class Delete(DPMatchToken):
    def __init__(self, cost, text_old):
        super(Delete, self).__init__(cost, text_old, "")

    def __str__(self):
        return "delete({0!r}, {1})".format(self.key, self.cost)


class DPMatchResult(object):
    def __init__(self, key, text, tokens, end):
        self.key = key
        self.text = text
        self.matched = "".join([t.text for t in tokens])
        self.cost = sum([t.cost for t in tokens])
        self.start = end - len(self.matched)
        self.end = end
        self.tokens = tokens
        self.match_count = len([True for token in tokens
                                if isinstance(token, Match)])


class DPMatcher(object):
    def __init__(self, key, text):
        self.key = [""] + list(key)
        self.text = [""] + list(text)
        self.C = self.make_table()

    def cost(self, c_key, c_text, i, j):
        return 1

    def make_table(self):
        """スコアテーブルの作成"""
        C = [([None] * len(self.text)) for i in xrange(len(self.key))]
        for j, c in enumerate(self.text):
            C[0][j] = 0
        for i, c in enumerate(self.key):
            C[i][0] = i

        for i in xrange(1, len(self.key)):
            c_key = self.key[i]
            for j in xrange(1, len(self.text)):
                c_text = self.text[j]
                if c_key == c_text:
                    C[i][j] = C[i-1][j-1]
                else:
                    up_cost = C[i-1][j] + self.cost(c_key, "", i, j)
                    left_cost = C[i][j-1] + self.cost("", c_text, i, j)
                    down_cost = C[i-1][j-1] + self.cost(c_key, c_text, i, j)
                    C[i][j] = min(up_cost, left_cost, down_cost)

        return C

    def print_table(self):
        """スコアテーブルの表示"""
        print("      " + "   ".join(self.text))
        for i, c in enumerate(self.key):
            print(" %1s  " % c, end='')
            for j in xrange(len(self.text)):
                w = self.C[i][j]
                if w is None:
                    print(" *  ", end='')
                else:
                    print(" %-3d" % w, end='')
            print()

    def traverse(self, i, j):
        if i == 0 or j == 0:
            return []

        base_cost = self.C[i][j]
        up_cost = self.C[i-1][j]
        left_cost = self.C[i][j-1]
        down_cost = self.C[i-1][j-1]

        min_cost = min(up_cost, left_cost, down_cost)

        if min_cost == down_cost:
            rv = self.traverse(i-1, j-1)
            if down_cost == base_cost:
                return rv + [Match(0, self.text[j])]
            else:
                cost = base_cost - down_cost
                return rv + [Replace(cost, self.key[i], self.text[j])]
            return rv
        elif min_cost == left_cost:
            rv = self.traverse(i, j-1)
            return rv + [Insert(base_cost - left_cost, self.text[j])]
        else:
            rv = self.traverse(i-1, j)
            return rv + [Delete(base_cost - up_cost, self.key[i])]

    def match(self):
        # マッチ箇所取り出し処理のスタート位置決定
        end = len(self.text) - 1
        for j in xrange(len(self.text) - 1, 0, -1):
            if self.C[-1][j] < self.C[-1][end]:
                 end = j

        # マッチ箇所取り出し
        results = self.traverse(len(self.key) - 1, end)
        return DPMatchResult("".join(self.key), "".join(self.text),
                             results, end)


def main():
    key = "survey"
    text = "foosurgerybar"

    matcher = DPMatcher(key, text)
    matcher.print_table()
    print()

    result = matcher.match()

    print(result.matched)
    for t in result.tokens:
        print(t)


if __name__ == '__main__':
    main()
