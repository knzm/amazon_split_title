# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
from collections import defaultdict
import ctypes

try:
    import svm
except ImportError:
    sys.path.insert(0, 'libsvm-python')

import svm


def get_ngram(s, n):
    for i in xrange(len(s) - n + 1):
        yield i, s[i:i+n]


def extract_features(text):
    backward_features = defaultdict(set)
    current_features = defaultdict(set)
    forward_features = defaultdict(set)

    # uni-gram
    for i, s in get_ngram(text, 1):
        forward_features[i].add(s)
        backward_features[i+1].add(s)

    # bi-gram
    for i, s in get_ngram(text, 2):
        forward_features[i].add(s)
        current_features[i+1].add(s)
        backward_features[i+2].add(s)

    # tri-gram
    for i, s in get_ngram(text, 3):
        forward_features[i].add(s)
        current_features[i+1].add(s)
        current_features[i+2].add(s)
        backward_features[i+3].add(s)

    features = []
    for i in xrange(len(text)+1):
        features.append({
                "forward": sorted(forward_features[i]),
                "current": sorted(current_features[i]),
                "backward": sorted(backward_features[i]),
                })
    return features


def make_vector(feature, word_ids):
    vec = {}
    for w in feature['forward']:
        wid = word_ids.get(w)
        if wid is None:
            continue
        vec[wid * 4] = 1
        vec[wid * 4 + 1] = 1
    for w in feature['current']:
        wid = word_ids.get(w)
        if wid is None:
            continue
        vec[wid * 4] = 1
        vec[wid * 4 + 3] = 1
    for w in feature['backward']:
        wid = word_ids.get(w)
        if wid is None:
            continue
        vec[wid * 4] = 1
        vec[wid * 4 + 2] = 1
    return vec


def read_words(path):
    words = {}
    with open(path) as f:
        for line in f:
            num, s = line.rstrip('\n').split(':', 1)
            words[int(num)] = s.decode('utf-8')
    return words


def save_words(path, word_ids):
    with open(path, 'w') as wf:
        for w, wid in sorted(word_ids.items(), key=lambda x: x[1]):
            print(u"{0}:{1}".format(wid, w).encode('utf-8'), file=wf)


def get_feature_weight(m):
    weight = defaultdict(int)
    for sv, coef in zip(m.get_SV(), m.get_sv_coef()):
        for fid, count in sv.items():
            weight[fid] += coef[0] * count
    return weight


def svm_predict_values(svm_model, x):
    if svm_model.is_probability_model():
        predict_func = svm.libsvm.svm_predict_probability
    else:
        predict_func = svm.libsvm.svm_predict_values

    values = (ctypes.c_double * 1)()
    x, idx = svm.gen_svm_nodearray(x)
    label = predict_func(svm_model, x, values)
    return label, values[0]
