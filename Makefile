.PHONEY: all dataset train predict

src_dir = data
dest_dir = data

# sample data
titles_csv = $(src_dir)/titles1.csv
titles2_csv = $(src_dir)/titles2.csv

# intermediate files
dataset_json = $(dest_dir)/dataset.json
words_txt = $(dest_dir)/words.txt
models = \
  $(dest_dir)/category-0.model \
  $(dest_dir)/category-1.model \
  $(dest_dir)/category-2.model \
  $(dest_dir)/category-3.model

python = python

all: $(words_txt) $(models)

clean:
	@rm -f $(dataset_json) $(words_txt) $(models)

$(dataset_json): $(titles_csv) make_dataset.py
	make dataset

$(words_txt) $(models): $(dataset_json) train.py
	make train

dataset: $(titles_csv)
	$(python) make_dataset.py $(titles_csv) $(dataset_json)

train: $(dataset_json)
	time $(python) train.py $(dataset_json)

predict: $(words_txt) $(models)
	@$(python) predict.py $(titles_csv)

eval: $(words_txt) $(models)
	@$(python) predict.py $(titles2_csv)
