# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
import collections
import csv

try:
    import svm
except ImportError:
    sys.path.insert(0, 'libsvm-python')

from svmutil import svm_load_model

from common import read_words, get_feature_weight


def main(model_path, words):
    m = svm_load_model(model_path)
    weight = get_feature_weight(m)

    pos_label = {
        0: "a",
        1: "f",
        2: "c",
        3: "b",
        }

    writer = csv.writer(sys.stdout)
    for num in sorted(weight.keys(), key=weight.__getitem__, reverse=True):
        if num == -1:
            continue
        pos = pos_label[num % 4]
        row = [
            str(weight[num]),
            pos,
            {"a": "*", "f": "|", "c": "", "b": "*"}[pos],
            words[num/4].encode('utf-8'),
            {"a": "*", "f": "*", "c": "", "b": "|"}[pos],
            ]
        writer.writerow(row)


if __name__ == '__main__':
    words = read_words(sys.argv[1])
    main(sys.argv[2], words)
